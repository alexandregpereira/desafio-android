package com.uol.desafioandroid.activity;

import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.uol.desafioandroid.R;
import com.uol.desafioandroid.databinding.ActivityDetailBinding;
import com.uol.desafioandroid.model.Shot;

public class DetailActivity extends AppCompatActivity {

    private static final String SHOT_KEY = "SHOT_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ActivityDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_detail);
        binding.toolbar.setNavigationIcon(R.drawable.ic_arrow_back);
        binding.toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        Shot shot = getIntent().getExtras().getParcelable(SHOT_KEY);
        binding.setShot(shot);
    }

    static void start(Context context, Shot shot){
        Intent intent = new Intent(context, DetailActivity.class);
        intent.putExtra(SHOT_KEY, shot);
        context.startActivity(intent);
    }
}
