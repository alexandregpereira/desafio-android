package com.uol.desafioandroid.activity;

import android.app.Dialog;
import android.arch.lifecycle.LifecycleRegistry;
import android.arch.lifecycle.LifecycleRegistryOwner;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.uol.desafioandroid.R;
import com.uol.desafioandroid.adapter.BaseAdapter;
import com.uol.desafioandroid.adapter.ShotAdapter;
import com.uol.desafioandroid.databinding.ActivityMainBinding;
import com.uol.desafioandroid.databinding.DialogSortBinding;
import com.uol.desafioandroid.model.Shot;
import com.uol.desafioandroid.util.MessageUtil;
import com.uol.desafioandroid.viewmodel.ShotViewModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements BaseAdapter.OnClickListener<Shot>,
        LifecycleRegistryOwner, ShotAdapter.OnBottomItemListener {

    private final LifecycleRegistry lifecycleRegistry = new LifecycleRegistry(this);
    private final ShotAdapter mAdapter = new ShotAdapter(new ArrayList<Shot>(), this, this);

    private ShotViewModel model;
    private Dialog sortDialog;
    private ActivityMainBinding mBinding;
    private MenuItem mMenuSortItem;
    private boolean showSortOption;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        setSupportActionBar(mBinding.toolbar);

        model = ViewModelProviders.of(this).get(ShotViewModel.class);

        mBinding.recyclerShot.setHasFixedSize(true);
        mBinding.recyclerShot.setLayoutManager(new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL));
        mBinding.recyclerShot.setAdapter(mAdapter);

        observeShotList();

        model.getErrorObservable().observe(this, new Observer<Integer>() {
            @Override
            public void onChanged(@Nullable @StringRes Integer errorMessage) {
                if(errorMessage == null) return;
                Log.d(ShotViewModel.TAG, "Main activity: erro");
                handleSwipe(model, mBinding.swipeRefresh);
                Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_LONG).show();
            }
        });

        handleSwipe(model, mBinding.swipeRefresh);

        mBinding.swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refresh();
            }
        });
    }

    private void observeShotList(){
        model.getNextShotListObservable().removeObservers(this);
        model.clearNextShotListObservable();
        model.getShots().observe(this, new Observer<List<Shot>>() {
            @Override
            public void onChanged(@Nullable List<Shot> shots) {
                if(shots == null) return;
                Log.d(ShotViewModel.TAG, "Main activity: recebido");
                if(!showSortOption && !shots.isEmpty()){
                    showSortOption = true;
                    if(mMenuSortItem != null) mMenuSortItem.setVisible(true);
                }
                mAdapter.setItems(shots);
                handleSwipe(model, mBinding.swipeRefresh);
                model.getShotListObservable().removeObserver(this);
                observeNextPage();
            }
        });
    }

    private void observeNextPage(){
        model.getNextShotListObservable().observe(this, new Observer<List<Shot>>() {
            @Override
            public void onChanged(@Nullable List<Shot> shots) {
                if(shots == null || shots.isEmpty()) return;
                Log.d(ShotViewModel.TAG, "Main activity: recebido next page");
                mAdapter.addItems(shots);
            }
        });
    }

    private void refresh(){
        model.clearCacheAndPage();
        observeShotList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        mMenuSortItem = menu.findItem(R.id.action_sort);
        mMenuSortItem.setVisible(showSortOption);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_sort:
                showSortDialog();
                break;
        }

        return true;
    }

    private void showSortDialog() {
        if(sortDialog == null){
            DialogSortBinding dialogSortBinding = DataBindingUtil.inflate(getLayoutInflater(), R.layout.dialog_sort, null, false);
            switch (model.getSort()){
                case ShotViewModel.POPULARITY_SORT:
                    dialogSortBinding.radioPopularity.setChecked(true);
                    break;
                case ShotViewModel.COMMENTS_SORT:
                    dialogSortBinding.radioComments.setChecked(true);
                    break;
                case ShotViewModel.RECENT_SORT:
                    dialogSortBinding.radioRecent.setChecked(true);
                    break;
                case ShotViewModel.VIEWS_SORT:
                    dialogSortBinding.radioViews.setChecked(true);
                    break;
            }

            dialogSortBinding.radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                    switch (i){
                        case R.id.radioComments:
                            model.setSort(ShotViewModel.COMMENTS_SORT);
                            break;
                        case R.id.radioRecent:
                            model.setSort(ShotViewModel.RECENT_SORT);
                            break;
                        case R.id.radioPopularity:
                            model.setSort(ShotViewModel.POPULARITY_SORT);
                            break;
                        case R.id.radioViews:
                            model.setSort(ShotViewModel.VIEWS_SORT);
                            break;
                    }
                    refresh();
                    handleSwipe(model, mBinding.swipeRefresh);
                    sortDialog.dismiss();
                }
            });
            sortDialog = MessageUtil.buildDialog(this, dialogSortBinding.getRoot());
        }

        sortDialog.show();
    }

    private void handleSwipe(ShotViewModel model, SwipeRefreshLayout swipeRefresh){
        if(model.isLoading() && !swipeRefresh.isRefreshing()){
            Log.d(ShotViewModel.TAG, "Show swipeRefresh");
            swipeRefresh.setRefreshing(true);
        }
        else if(!model.isLoading() && swipeRefresh.isRefreshing()){
            Log.d(ShotViewModel.TAG, "Dismiss swipeRefresh");
            swipeRefresh.setRefreshing(false);
        }
    }

    @Override
    public LifecycleRegistry getLifecycle() {
        return lifecycleRegistry;
    }

    @Override
    public void onBottom() {
        Log.d(ShotViewModel.TAG, "nextPage");
        model.nextPage();
    }

    @Override
    public void onClicked(Shot shot) {
        DetailActivity.start(this, shot);
    }
}
