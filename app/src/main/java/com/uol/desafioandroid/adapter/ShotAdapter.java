package com.uol.desafioandroid.adapter;

import com.uol.desafioandroid.R;
import com.uol.desafioandroid.databinding.ItemShotBinding;
import com.uol.desafioandroid.model.Shot;

import java.util.List;

/**
 * Created by Alexandre on 01/07/2017.
 *
 */

public class ShotAdapter extends BaseAdapter<Shot, ItemShotBinding>{

    private static final int BOTTOM_TYPE = 22;
    private static final int NORMAL_TYPE = 23;
    private final OnBottomItemListener mOnBottomItemListener;

    public interface OnBottomItemListener{
        void onBottom();
    }

    public ShotAdapter(List<Shot> items, OnBottomItemListener onBottomItemListener, OnClickListener<Shot> listener) {
        super(items, R.layout.item_shot, listener);
        mOnBottomItemListener = onBottomItemListener;
    }

    @Override
    public void onBindViewHolder(ViewHolder<Shot, ItemShotBinding> holder, int position) {
        super.onBindViewHolder(holder, position);
        if(getItemViewType(position) == BOTTOM_TYPE) mOnBottomItemListener.onBottom();
    }

    @Override
    protected void onBindViewHolder(ItemShotBinding itemShotBinding, Shot shot) {
        itemShotBinding.setShot(shot);
    }

    @Override
    public int getItemViewType(int position) {
        if (isPositionBottom(position))
            return BOTTOM_TYPE;

        return NORMAL_TYPE;
    }

    private boolean isPositionBottom(int position) {
        return getItems().size() - 2 == position + 1;
    }
}
