package com.uol.desafioandroid.api;

import com.uol.desafioandroid.model.Shot;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

/**
 * Created by Alexandre on 01/07/2017.
 *
 * Retrofit interface para consumir as Shot APIs
 */

public interface ShotApi {

    @Headers("Authorization: Bearer 6f55cea6a1bffcadac800cfb7bb9bec423d96a61ed6d7e08177875f13c99e314")
    @GET("shots")
    public Call<List<Shot>> getShots(@Query("page") int page, @Query("sort") String sort, @Query("per_page") int perPage);
}
