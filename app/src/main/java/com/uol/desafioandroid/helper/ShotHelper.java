package com.uol.desafioandroid.helper;

import android.databinding.BindingAdapter;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Build;
import android.text.Html;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

/**
 * Created by Alexandre on 01/07/2017.
 *
 */

public final class ShotHelper {

    @BindingAdapter({"buildHtml"})
    public static void buildHtml(TextView textView, String html){
        if(html == null || html.isEmpty()) return;
        if(Build.VERSION.SDK_INT >= 24) {
            textView.setText(Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY));
        }
        else {
            textView.setText(Html.fromHtml(html));
        }
    }

    @BindingAdapter({"loadImageUrl"})
    public static void loadImageUrl(ImageView imageView, String url){
        if(url == null || url.isEmpty()) return;
        Picasso.with(imageView.getContext())
                .load(url)
                .fit().centerCrop()
                .into(imageView);
    }

    @BindingAdapter({"loadImageUrlWithTransformation"})
    public static void loadImageUrlWithTransformation(ImageView imageView, String url){
        if(url == null || url.isEmpty()) return;
        Picasso.with(imageView.getContext())
                .load(url)
                .transform(getTransformation())
                .into(imageView);
    }

    private static Transformation getTransformation(){
        return new Transformation() {
            @Override
            public Bitmap transform(Bitmap source) {
                Bitmap.Config config = source.getConfig();
                if(config == null) return source;

                int size = Math.min(source.getWidth(), source.getHeight());

                int x = (source.getWidth() - size) / 2;
                int y = (source.getHeight() - size) / 2;

                Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
                if (squaredBitmap != source) {
                    source.recycle();
                }

                Bitmap bitmap = Bitmap.createBitmap(size, size, config);

                Canvas canvas = new Canvas(bitmap);
                Paint paint = new Paint();
                BitmapShader shader = new BitmapShader(squaredBitmap,
                        BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
                paint.setShader(shader);
                paint.setAntiAlias(true);

                float r = size / 2f;
                canvas.drawCircle(r, r, r, paint);

                squaredBitmap.recycle();
                return bitmap;
            }

            @Override
            public String key() {
                return "circle";
            }
        };
    }
}
