package com.uol.desafioandroid.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;

/**
 *
 * Created by Alexandre on 05/05/2017.
 */

public final class MessageUtil {

    @SuppressWarnings("ConstantConditions")
    public static Dialog buildDialog(@NonNull Context context, @NonNull View view) {
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(view);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        return dialog;
    }
}
