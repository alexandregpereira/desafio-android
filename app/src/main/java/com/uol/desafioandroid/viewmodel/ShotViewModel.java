package com.uol.desafioandroid.viewmodel;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.annotation.StringRes;
import android.util.Log;

import com.uol.desafioandroid.R;
import com.uol.desafioandroid.api.ShotApi;
import com.uol.desafioandroid.model.Shot;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Alexandre on 01/07/2017.<BR><BR>
 *
 * Esta classe é uma representação do novo componente de arquitetura do google
 */
public class ShotViewModel extends ViewModel{

    public static final String TAG = "Shot";

    public static final String POPULARITY_SORT = "popularity";
    public static final String COMMENTS_SORT = "comments";
    public static final String RECENT_SORT = "recent";
    public static final String VIEWS_SORT = "views";


    @StringDef(value = {POPULARITY_SORT, COMMENTS_SORT,
            RECENT_SORT, VIEWS_SORT})
    @Retention(RetentionPolicy.SOURCE)
    private @interface SortFlags {}

    private final MutableLiveData<List<Shot>> shotListObservable = new MutableLiveData<>();
    private final MutableLiveData<List<Shot>> nextShotListObservable = new MutableLiveData<>();
    private final MutableLiveData<Integer> errorObservable = new MutableLiveData<>();
    private final ShotApi mShotApi;
    private boolean loading;
    private boolean cached;
    @SortFlags
    private String sort = POPULARITY_SORT;
    private int mPage = 1;

    public ShotViewModel(){
        super();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://api.dribbble.com/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        mShotApi = retrofit.create(ShotApi.class);
    }

    public LiveData<List<Shot>> getShots(){
        return getShots(sort);
    }

    private LiveData<List<Shot>> getShots(@SortFlags String sort){
        if(!isLoading() && !cached) {
            this.sort = sort;
            loading = true;
            mShotApi.getShots(mPage, sort, 30).enqueue(new Callback<List<Shot>>() {
                @Override
                public void onResponse(@NonNull Call<List<Shot>> call, @NonNull Response<List<Shot>> response) {
                    dispatchShotList(response.body(), mPage);
                }

                @Override
                public void onFailure(@NonNull Call<List<Shot>> call, @NonNull Throwable t) {
                    loading = false;
                    t.printStackTrace();
                    Log.e(TAG, "onFailure: " + t.getMessage());
                    errorObservable.setValue(getErrorMessage(t));
                    errorObservable.setValue(null);
                }
            });
        }

        return shotListObservable;
    }

    private void dispatchShotList(@Nullable List<Shot> shotList, int page) {
        loading = false;
        if (shotList == null) {
            Log.e(TAG, "Null list");
            return;
        }
        Log.d(TAG, "Page " + page + " Recebido: " + shotList.size());
        List<Shot> shotObservableList = shotListObservable.getValue();
        if(shotObservableList != null && page > 1){
            shotObservableList.addAll(shotList);
            nextShotListObservable.setValue(shotList);
        }
        else shotObservableList = shotList;

        shotListObservable.setValue(shotObservableList);
        cached = true;
    }

    @StringRes
    private int getErrorMessage(Throwable t){
        if(t instanceof UnknownHostException){
            return R.string.no_connection_shot;
        }

        if(t instanceof SocketTimeoutException){
            return R.string.no_connection_shot;
        }

        return R.string.unknown_error_shot;
    }

    public MutableLiveData<Integer> getErrorObservable() {
        return errorObservable;
    }

    public boolean isLoading() {
        return loading;
    }

    public void clearCacheAndPage(){
        shotListObservable.setValue(null);
        mPage = 1;
        this.cached = false;
    }

    public void nextPage() {
        cached = false;
        ++mPage;
        getShots(sort);
    }

    public void setSort(@SortFlags String mSort) {
        this.sort = mSort;
    }

    public String getSort() {
        return sort;
    }

    public LiveData<List<Shot>> getNextShotListObservable() {
        return nextShotListObservable;
    }

    public void clearNextShotListObservable() {
        nextShotListObservable.setValue(null);
    }

    public LiveData<List<Shot>> getShotListObservable() {
        return shotListObservable;
    }
}
